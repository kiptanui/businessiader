package appconfig

type AppConfig struct {
	CrmDb_Conn            string
	Nats_Server_URL       string
	Nats_Cluster_Id       string
	Nats_Client_Id        string
	Seq_Server            string
	Seq_Server_Key        string
	PG_Portal_Endpoint    string
	Billing_Endpoint      string
	MeteringServ_Endpoint string
}

var _configs AppConfig

func SetConfig(conf AppConfig) {
	_configs = conf
}

func GetConfig() AppConfig {
	return _configs
}
