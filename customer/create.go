package customer

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"powergen/crm/data"

	"powergen/crm/appconfig"

	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	"github.com/muhoro/log"
)

type checkedDuplicated struct {
	FirstName      string
	LastName       string
	Surname        string
	Identification string
}
type meterVerifyResp struct {
	Status   string   `json:"status"`
	MeterNo  string   `json:"meter_number"`
	SiteCode string   `json:"site_code"`
	Errors   []string `json:"errors"`
}

type responseRegistration struct {
	Errors []string
}

type responseValidation meterVerifyResp
type CustomerRegPayload struct {
	FirstName        string
	LastName         string
	IdentificationNo string
	PrimaryPhone     string
	AlternatePhone   string
	Village          string
	HouseNo          string
	Surname          string
}
type custRegbillingPayload struct {
	Id        string `json:"cust_id"`
	FirstName string `json:"cust_firstname"`
	LastName  string `json:"cust_lastname"`
	Surname   string `json:"cust_surname"`
	PhoneNo   string `json:"phone_number"`
	MeterNo   string `json:"meter_no"`
	SiteName  string `json:"site_name"`
	Village   string `json:"village"`
	HouseNo   string `json:"house_number"`
	SiteCode  string `json:"site_code"`
	TurretNo  string `json:"turret_no"`
	BreakerNo string `json:"breaker_no"`
}

type MessageStatus struct {
	Status  bool
	Message string
}
type responseBilling struct {
	Status string
	Errors []string
}

var res []meterVerifyResp

func validateMeter(meter string, sitecode string, ch chan meterVerifyResp) {
	client := &http.Client{}
	response, err := client.Get(appconfig.GetConfig().MeteringServ_Endpoint + "/api/verifycustomerdata?meter=" + meter + "&sitecode=" + sitecode)
	if err != nil {
		return
	}
	resp := meterVerifyResp{}
	decoder := json.NewDecoder(response.Body)
	if err := decoder.Decode(&resp); err != nil {
		return
	}
	ch <- resp
}
func MassUpload(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	db := r.Context().Value("database").(*gorm.DB)

	file, _, err := r.FormFile("uploadfile")
	if err != nil {
		log.Error(err.Error(), nil)
		fmt.Println(err)
		log.Error(err.Error(), nil)
		return
	}
	defer file.Close()
	var verificationResponses []meterVerifyResp
	records, _ := csv.NewReader(file).ReadAll()
	ch := make(chan meterVerifyResp)
	for i, row := range records {
		if i == 0 {
			continue
		}
		go validateMeter(row[10], row[12], ch)
	}
	x := 0
	for range records {
		x += 0
		if x == 0 {
			x++
			continue
		}

		r := <-ch
		verificationResponses = append(verificationResponses, r)

	}
	var regResponse responseRegistration
	for _, v := range verificationResponses {
		if len(v.Errors) > 0 {
			regResponse.Errors = append(regResponse.Errors, v.Errors...)
		}
	}
	if len(regResponse.Errors) > 0 {
		w.Header().Add("Content-Type", "json/application")
		w.WriteHeader(http.StatusBadRequest)
		d, _ := json.Marshal(&regResponse)
		w.Write(d)
		return
	}

	var custRegPayload []custRegbillingPayload
	for y, line := range records {

		if y == 0 {
			continue
		}

		cust := data.Customer{
			Id:               uuid.New(),
			FirstName:        line[0],
			LastName:         line[1],
			Surname:          line[2],
			IdentificationNo: line[3],
			PhoneNos:         "{" + line[4] + "}",
			Gender:           line[6],
			Dob:              line[7],
			//Language:         line[8],
			Village: line[11],
			House:   line[9],
		}
		er := db.Create(&cust).Error
		if er != nil {
			log.Error(er.Error(), nil)
		}

		sparkcust := custRegbillingPayload{
			Id:        cust.Id.String(),
			FirstName: cust.FirstName,
			LastName:  cust.LastName,
			Surname:   cust.Surname,
			Village:   cust.Village,
			HouseNo:   cust.House,
			MeterNo:   line[10],
			SiteCode:  line[12],
			PhoneNo:   "+" + line[4],
		}
		custRegPayload = append(custRegPayload, sparkcust)

	}
	regRes := responseBilling{}
	_, er := postJSON(appconfig.GetConfig().Billing_Endpoint+"/api/accounts/energy", &custRegPayload, &regRes)
	if regRes.Errors != nil {
		fmt.Println(er)
		fmt.Println(regRes.Errors)
		return
	}

}
func post(req *http.Request) ([]byte, error) {

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	regRes := responseBilling{}
	fmt.Println(string(body))
	s := json.Unmarshal(body, &regRes)
	fmt.Println(s)
	return body, nil
}
func postJSON(path string, postBody, postResponse interface{}) ([]byte, error) {
	data, err := json.Marshal(postBody)
	//	println(string(data))
	httpClient := &http.Client{}

	req, err := http.NewRequest("POST", path, bytes.NewBuffer(data))
	if err != nil {
		log.Fatal(err.Error(), nil)
	}
	req.Header.Add("Content-Type", "application/json")

	resp, err := httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	d, _ := ioutil.ReadAll(resp.Body)
	// if resp.StatusCode != 200 && resp.StatusCode != 201 {
	// 	return nil, errors.New(string(d))
	// }

	json.Unmarshal(d, postResponse)

	return d, nil
}

func getVerification(line string, ch chan responseValidation) {
	response := responseValidation{}
	resp, err := http.Get("/")
	body, _ := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Error(err.Error(), &body)
		return
	}
	json.Unmarshal(body, &response)
	ch <- response

}
func getResponse(ch chan<- string, onExit func()) {
	go func() {
		defer onExit()
		client := &http.Client{}
		req, _ := http.NewRequest("GET", "http://test.powergen-re.net/api/metering/get/verification?sitecode=+meter", nil)
		req.Header.Set("Accept", "application/json")
		res, err := client.Do(req)
		if err != nil {
			panic(err) // should be handled properly
		}
		defer res.Body.Close()
		body, err := ioutil.ReadAll(res.Body)
		ch <- string(body)
	}()
}
func getResponse1(sitecode string, meter string, ch chan meterVerifyResp) {
	resp := meterVerifyResp{}
	client := &http.Client{}
	req, _ := http.NewRequest("GET", "http://test.powergen-re.net/api/metering/get/verification?sitecode="+sitecode+"&meter="+meter, nil)
	req.Header.Set("Accept", "application/json")
	res, err := client.Do(req)
	if err != nil {
		panic(err) // should be handled properly
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	json.Unmarshal(body, res)

	ch <- resp

}
