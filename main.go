package main

import (
	"context"
	"powergen/crm/appconfig"

	"net/http"
	"os"
	"powergen/crm/customer"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/muhoro/log"
)

type ContextInjector struct {
	ctx context.Context
	h   http.HandlerFunc
}

func (ci *ContextInjector) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ci.h.ServeHTTP(w, r.WithContext(ci.ctx))
	enableCors(&w)
}
func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}
func buildConfigs(use_docker bool) appconfig.AppConfig {
	var conf appconfig.AppConfig
	if use_docker {
		conf = appconfig.AppConfig{
			CrmDb_Conn:            os.Getenv("connection_string"),
			Nats_Server_URL:       os.Getenv("nats_server"),
			Nats_Cluster_Id:       os.Getenv("nats_cluster"),
			Seq_Server:            os.Getenv("seq_server"),
			Seq_Server_Key:        os.Getenv("seq_server_key"),
			PG_Portal_Endpoint:    os.Getenv("pg_portal_endpoint"),
			Nats_Client_Id:        os.Getenv("client_id"),
			Billing_Endpoint:      os.Getenv("billing_endpoint"),
			MeteringServ_Endpoint: os.Getenv("metering_endpoint"),
		}
	} else {
		conf = appconfig.AppConfig{
			CrmDb_Conn: "host=localhost port=5432 user=postgres dbname=crm password=root sslmode=disable",

			Nats_Cluster_Id:       "test-cluster",
			Seq_Server:            "http://localhost:5341",
			Seq_Server_Key:        "key",
			PG_Portal_Endpoint:    "http://keuatportal.powergen-re.net",
			Nats_Client_Id:        "crm-service",
			Billing_Endpoint:      "https://sl.powergen-re.net/billing",
			MeteringServ_Endpoint: "https://sl.powergen-re.net/metering",
		}
	}
	appconfig.SetConfig(conf)
	return conf
}
func main() {

	use_docker := true

	buildConfigs(use_docker)
	log.BuildLogger("CRM").UseSeq(appconfig.GetConfig().Seq_Server, appconfig.GetConfig().Seq_Server_Key)
	db, err := gorm.Open("postgres", appconfig.GetConfig().CrmDb_Conn)
	if err != nil {
		log.Fatal(err.Error(), nil)
	}

	db, er := gorm.Open("postgres", appconfig.GetConfig().CrmDb_Conn)
	if err != nil {
		panic(er.Error())
	}

	ctx := context.WithValue(context.Background(), "database", db)

	router := mux.NewRouter()

	router.Handle("/crm/api/registerCustomers", &ContextInjector{ctx, customer.MassUpload}).Methods("POST")
	router.Handle("/crm/api/getCustomerbyId", &ContextInjector{ctx, customer.GetCustomerbyId}).Methods("GET")
	router.Handle("/crm/api/customers", &ContextInjector{ctx, customer.FetchCustomers}).Methods("GET")
	router.Handle("/api/languages", &ContextInjector{ctx, customer.GetLanguages}).Methods("GET")
	router.Handle("/crm/api/getcustomerbyphonenumber", &ContextInjector{ctx, customer.GetCustomerbyPhone}).Methods("GET")
	router.Handle("/crm/api/customer", &ContextInjector{ctx, customer.GetCustomerSearch}).Methods("GET")
	router.Handle("/crm/api/updateCustomer", &ContextInjector{ctx, customer.UpdateCustomer}).Methods("POST")
	router.HandleFunc("/crm/api/deleteCustomer", customer.DeleteCustomer).Methods("DELETE")
	log.Fatal(http.ListenAndServe(":6001", router).Error(), router)

}
