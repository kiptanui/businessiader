FROM alpine:latest as builder
MAINTAINER Isaac Kiptanui <ikiptanui@powergen-re.com>
RUN apk add go && \
    apk add musl-dev
ENV GOPATH=/go
ENV PATH=$PATH:$GOPATH/bin
ADD . $GOPATH/src/powergen/crm
WORKDIR $GOPATH/src/powergen/crm
RUN go build -o app .

FROM alpine:latest
RUN apk update && \
    apk add ca-certificates && \
    apk add bash
COPY --from=builder /go/src/powergen/crm/app .
EXPOSE 8000
EXPOSE 5432
EXPOSE 5341
CMD ./app